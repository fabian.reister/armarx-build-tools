# -*- coding: utf-8 -*-
# pylint: disable=R1708
from __future__ import unicode_literals

from cmake_format import configuration
from cmake_format import lexer
from cmake_format import parse
from cmake_format import parse_funs
from cmake_format.parse.printer import tree_string, test_string
from cmake_format.parse.common import NodeType

from rich.console import Console

console = Console()


class CMakeParser:
    def __init__(self, content: str) -> None:
        self._content = content

        # init
        self._config = configuration.Configuration()
        self._parse_db = parse_funs.get_parse_db()
        self._parse_ctx = parse.ParseContext(self._parse_db)

        # setup

        self._config.parse.fn_spec.add(
            'armarx_project',
            flags=[],
            kwargs={},
        )

        self._config.parse.fn_spec.add(
            'depends_on_armarx_package',
            flags=[],
            kwargs={},
        )

        self._parse_ctx.parse_db.update(
            parse_funs.get_legacy_parse(self._config.parse.fn_spec).kwargs)

        self.__parse_content()

    def __parse_content(self):


        print("parsing content: ", content)

        self._tokens = lexer.tokenize(self._content)

        console.print("tokens")
        console.print(self._tokens)

        self._fst_root = parse.parse(self._tokens, self._parse_ctx)

        console.print("fst_root")
        console.print(self._fst_root)

        for node in self._fst_root.children:
            if node.node_type is NodeType.STATEMENT:
                console.print(node)
                console.print(node.children)

                for part in node.children:
                    if part.node_type is NodeType.FUNNAME:
                        print(part.name)

    def armarx_dependencies(self):
        pass


content = """\
      # The following multiple newlines should be collapsed into a single newline




      cmake_minimum_required(VERSION 2.8.11)
      project(cmake_format_test)
      """

if __name__ == "__main__":

    fn = "/home/fabi/repos/RobotComponents/CMakeLists.txt"

    with open(fn, "r") as f:
        content = f.read()

    

    cmake_parser = CMakeParser(content)
