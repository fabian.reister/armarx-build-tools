import os

import click
from rich.tree import Tree

from armarx_build.core.package_manager import PackageManager
from armarx_build.cli import cli


@cli.group("config")
def cli_config():
    pass


@cli_config.command("edit")
@click.pass_obj
def cli_config_edit(pacman: PackageManager):

    # https://click.palletsprojects.com/en/7.x/utils/#finding-application-folders

    app_dir = click.get_app_dir(pacman.APP_NAME)

    if not os.path.exists(app_dir):
        os.makedirs(app_dir)

    click.edit(filename=pacman.config_filename)


@cli_config.command("show")
@click.pass_obj
def cli_config_show(pacman: PackageManager):
    cfg = pacman.config_filename

    if not os.path.exists(cfg):
        pacman.console.print("Config does not exist!", style="red")

    tree = Tree("Config")

    for section, vals in pacman.config.items():
        branch = tree.add(f"[blue] {section}")
        for k, v in vals.items():
            branch.add(f"[yellow]{k}[/yellow]: {v}")

    pacman.console.print(tree)