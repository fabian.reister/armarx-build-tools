import os

import click

from armarx_build.core.package_manager import PackageManager
from armarx_build.cli import cli


@cli.command("ide")
@click.option("--core", "-c", is_flag=True)
@click.option("--integration", "-i", is_flag=True)
@click.option("--skills" ,"-s", is_flag=True)
@click.pass_obj
def cli_ide(pacman: PackageManager, core: bool, integration: bool, skills: bool):

    packages = pacman.dependency_resolver.process_packages(pacman.packages,  pacman.packages)

    cmd = "code -n "
    for pkg in packages:
        cmd += f" -a {pkg.path}"

    os.system( cmd )