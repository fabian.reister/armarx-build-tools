import click

from armarx_build.cli import cli


@cli.command("migrate")
@click.argument("file")
def cli_migrate(file):

    from armarx_build.migrate import migrate_cmakelists
    migrate_cmakelists(file)