import os
from dataclasses import dataclass
from typing import List

import click
import git
import rich
from rich.table import Table

from armarx_build.core.types import Package
from armarx_build.core.package_manager import PackageManager
from armarx_build.cli import cli


@cli.group("git")
def cli_git():
    pass


@cli_git.command("pull")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.pass_obj
def cli_git_pull(pacman: PackageManager, packages: List[str]):

    if not packages:
        packages = pacman.packages
    else:
        packages = pacman.package_finder.find(packages)

    table = Table("package", "active branch", "status")

    for package in packages:
        try:
            repo = git.Repo(package.path) # throws an exception if this is not a repo

            pacman.console.log(f"[yellow]Pulling repo {package.name}[/yellow]")
            cmake_cfg_cmd = f"cd {package.path} && git pull "
            result = os.system(cmake_cfg_cmd)
        except:
            pass


@cli_git.command("push")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.pass_obj
def cli_git_push(pacman: PackageManager, packages: List[str]):

    if not packages:
        packages = pacman.packages
    else:
        packages = pacman.package_finder.find(packages)

    table = Table("package", "active branch", "status")

    for package in packages:
        try:
            repo = git.Repo(package.path) # throws an exception if this is not a repo

            pacman.console.log(f"[yellow]Pushing commits in repo {package.name}[/yellow]")
            cmake_cfg_cmd = f"cd {package.path} && git push "
            result = os.system(cmake_cfg_cmd)
        except:
            pass


@cli_git.command("status")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.option("--skip-fetch", "-s", is_flag=True)
@click.pass_obj
def cli_git_status(pacman: PackageManager, packages: List[str],
                   skip_fetch: bool):

    if not packages:
        packages = pacman.packages
    else:
        packages = pacman.package_finder.find(packages)

    table = Table("package", "active branch", "status")

    for package in packages:
        rst = repo_status(package, skip_fetch)

        if rst is None:
            continue

        status = ""

        space = "   "

        if rst.clean:
            status = "[green]:heavy_check_mark:[/green]"
        else:
            status += space
            status += ":pencil: " if rst.dirty else space
            status += " :question:" if rst.untracked else space

        # rst.commit.author_tz_offset

        name = package.name
        active_branch = rst.active_branch

        if rst.incoming:
            active_branch = f"[blue]{active_branch}[/blue]"

        if rst.is_accessible:
            name = f"[green]{name}[/green]"

        table.add_row(name, active_branch, status)

    pacman.console.print(table)


@dataclass
class RepoStatus:
    active_branch: str

    commit: git.Commit
    untracked: List[str]
    dirty: bool

    incoming: bool

    has_upstream: bool

    is_accessible: bool

    @property
    def clean(self):
        return not (bool(self.untracked) or self.dirty)


def repo_status(package: Package, skip_fetch: bool):

    git_ssh_identity_file = os.path.expanduser(
        '~/.ssh/id_ed25519')  # or id_rsa
    git_ssh_cmd = 'ssh -i %s' % git_ssh_identity_file

    with git.Git().custom_environment(GIT_SSH_COMMAND=git_ssh_cmd):

        changed = False
        has_upstream = False
        is_accessible = False
        active_branch = ""

        try:
            repo = git.Repo(package.path)
            active_branch = repo.active_branch.name

            current_hash = repo.head.object.hexsha
            o = repo.remotes.origin

            if not skip_fetch:
                o.fetch()
                is_accessible = True

            try:
                branch_remote = o.refs[active_branch]
                changed = branch_remote.object.hexsha != current_hash

                #if changed:
                #    repo.active_branch.

                has_upstream = repo.active_branch.is_remote()
            except IndexError:  # does not exist on remote
                pass
        except git.GitCommandError:
            pass
        except git.exc.InvalidGitRepositoryError:
            return None

        return RepoStatus(
            active_branch=active_branch,
            commit=repo.head.commit,
            untracked=repo.untracked_files,
            dirty=repo.is_dirty(),
            incoming=changed,
            has_upstream=has_upstream,
            is_accessible=is_accessible,
        )