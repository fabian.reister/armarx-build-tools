import click
from rich.tree import Tree

from armarx_build.core.package_manager import PackageManager

from armarx_build.cli import cli

@cli.command("discover")
@click.option("--with-deps", is_flag=True)
@click.option("--raw", is_flag=True)
@click.option("--paths", is_flag=True)
@click.pass_obj
def cli_discover(pacman: PackageManager, with_deps: bool, raw: bool, paths: bool):

    packages = pacman.dependency_resolver.process_packages(pacman.packages,  pacman.packages)

    def print_nice():
        tree = Tree("Discovered projects")

        for pkg in packages:
            branch = tree.add(f"[green] {pkg.name}")

            if with_deps:
                for dep in pkg.dependencies.optional:
                    branch.add(f"[blue] {dep}")

                for dep in pkg.dependencies.optional:
                    branch.add(f"[yellow] {dep}")

        pacman.console.print(tree)

    def print_raw():
        for pkg in packages:
            print(pkg.name)

    def print_paths_raw():
        for pkg in packages:
            print(pkg.path)


    if not raw:
        print_nice()
        return

    if paths:
        print_paths_raw()
        return

    print_raw()