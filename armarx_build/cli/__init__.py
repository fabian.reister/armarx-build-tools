import click


from rich.console import Console

console = Console()


@click.group()
@click.pass_context
def cli(ctx):
    from armarx_build.core.package_manager import PackageManager
    ctx.obj = PackageManager(console=console)


from .build import *
from .config import *
from .dependency_tree import *
from .discover import *
from .git import *
from .ide import *
from .install import *
from .migrate import *
