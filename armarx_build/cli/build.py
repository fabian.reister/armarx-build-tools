import os
from typing import List, Tuple

import click

from armarx_build.core.package_manager import PackageManager

from armarx_build.cli import cli

@cli.command("build")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.option("-f", "--package-folder", 'folders', multiple=True, default=[])
@click.option("-d", "--default", is_flag=True)
@click.option("-a", "--all", is_flag=True)
@click.option("-c", "--configure", is_flag=True)
@click.option("-s", "--skip-dependencies", is_flag=True)
@click.option("--clean", is_flag=True)
@click.option("-t", "--this", is_flag=True)
@click.option("-n", "--no-deps", is_flag=True)
@click.option("-i", "--ignore-failure", is_flag=True)
@click.pass_obj
def cli_build(
    pacman: PackageManager,
    packages: List[str],
    folders: Tuple[str],
    default: bool,
    all: bool,
    configure: bool,
    skip_dependencies: bool,
    clean: bool,
    this: bool,
    no_deps: bool,
    ignore_failure: bool,
):
    folders = list(folders)

    if this:
        folders.append(os.getcwd())

    if folders:
        packages = pacman.packages

        packages_by_path = dict([(p.path, p) for p in packages])
        known_package_paths = packages_by_path.keys()

        def find_matching_package(filepath: str):
            for p in known_package_paths:
                if filepath.startswith(p):
                    return p

            return None

        mp = list()
        for pf in folders:
            matching_package_path = find_matching_package(pf)

            if matching_package_path:
                mp.append(packages_by_path[matching_package_path].name)

        packages = mp

    # if no package is specified, use default packages
    if not packages:
        if default or not all:
            packages = pacman.default_packages
        elif all:
            packages = pacman.packages
    else:
        packages = pacman.package_finder.find(packages)

    if not no_deps:
        packages = pacman.dependency_resolver.process_packages(
            packages, pacman.packages)

    build_order = list(
        pacman.dependency_resolver.build_order(packages))

    #tree = Tree("Build order")
    #[tree.add(pkg.name) for pkg in build_order]
    #console.print(tree)

    build_order_str = ""

    for pkg in build_order[:-1]:
        build_order_str += pkg.name + " :arrow_right: "
    build_order_str += build_order[-1].name

    if len(build_order) > 1:
        pacman.console.print(build_order_str)

    #from rich.progress import track

    failed_packages = []

    if not skip_dependencies and not no_deps:
        ext_deps = pacman.external_dependencies  #[1:]

        # console.print(ext_deps)

        for ext_dep in ext_deps:
            pacman.console.print("")
            pacman.console.log(f"Building dependency package {ext_dep}",
                        style="bold cyan")
            if pacman.builder.build(ext_dep,
                                    force_configure=configure,
                                    clean=clean):
                pacman.console.log(f"{ext_dep}: build finished.", style="bold green")
            else:
                pacman.console.log(f"{ext_dep}: build failed.", style="bold red")

                if ignore_failure:
                    failed_packages.append(ext_dep)
                else:
                    return


    ASSETS_DIR = os.path.join(os.path.dirname(__file__), "..", "data")

    print(ASSETS_DIR)

    #for pkg in track(build_order):
    for pkg in build_order:
        pacman.console.print("")
        pacman.console.log(f"Building package {pkg}", style="bold cyan")
        #with console.status(f"Building package '{pkg}'") as status:
        if pacman.builder.build(pkg, force_configure=configure, clean=clean):
            pacman.console.log(f"{pkg}: build finished.", style="bold green")
        else:
            pacman.console.log(f"{pkg}: build failed.", style="bold red")

            try:
                import notify2 # pip install notify2 dbus-python
                notify2.init("ArmarX")
                n = notify2.Notification(f" Building package '{pkg.name}' failed.",
                                "",
                                os.path.join(ASSETS_DIR, "ArmarXCore-Logo-failure.png")   # Icon name
                                )
                n.show()
            except:
                pass

            if ignore_failure:
                failed_packages.append(pkg)
            else:
                return

    try:
        import notify2 # pip install notify2 dbus-python
        notify2.init("ArmarX")
        n = notify2.Notification(" Build finished.",
                         "",
                         os.path.join(ASSETS_DIR,"ArmarXCore-Logo.svg" )  # Icon name
                        )
        n.show()
    except:
        pass

    # report if packages failed
    if failed_packages:
        pacman.console.log("\n\nThe following packages failed: ", style="bold red")
        [pacman.console.log(f"- {pkg.name}", style="red") for pkg in failed_packages]
