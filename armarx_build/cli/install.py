import os
from typing import List

import click
from rich.tree import Tree

from armarx_build.core.types import Package
from armarx_build.core.package_manager import PackageManager
from armarx_build.cli import cli


@cli.command("install")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.option("-t", "--this", is_flag=True)
@click.option("-n", "--no-deps", is_flag=True)
@click.pass_obj
def cli_install(
    pacman: PackageManager,
    packages: List[str],
    this: bool,
    no_deps: bool
):
    folders = []

    if this:
        folders.append(os.getcwd())

    # if no package is specified, use default packages
    if not packages:
        packages = pacman.default_packages
    else:
        packages = pacman.package_finder.find(packages)

    if not no_deps:
        packages = pacman.dependency_resolver.process_packages(
            packages, pacman.packages)


    apt_packages = []

    for package in packages:
        package: Package

        x_filename = os.path.join(package.path, "armarx.yaml")

        if not os.path.exists(x_filename):
            continue

        with open(x_filename, "r") as xf:
            import yaml
            xyml = yaml.load(xf.read())


            apt_pkgs = xyml["dependencies"]["required"]["ubuntu"]

            # console.print(apt_pkgs)

            for pkg in apt_pkgs:
                # import apt
                # TODO check if pkg.package is installed

                apt_packages.append(pkg["package"])

    pacman.console.print("Installing missing dependencies:", style="bold yellow")
    pacman.console.print()

    # console.print(apt_packages)

    tree = Tree("Required dependencies")
    [tree.add(pkg) for pkg in apt_packages]
    pacman.console.print(tree)
    pacman.console.print()

    cmd = "sudo apt install " + (" ".join(apt_packages))
    pacman.console.print(cmd)
    pacman.console.print()

    os.system(cmd)

    return

    build_order = list(
        pacman.dependency_resolver.build_order(packages))

    #tree = Tree("Build order")
    #[tree.add(pkg.name) for pkg in build_order]
    #console.print(tree)

    build_order_str = ""

    for pkg in build_order[:-1]:
        build_order_str += pkg.name + " :arrow_right: "
    build_order_str += build_order[-1].name

    if len(build_order) > 1:
        console.print(build_order_str)

    #from rich.progress import track

    if not skip_dependencies and not no_deps:
        ext_deps = pacman.external_dependencies  #[1:]

        # console.print(ext_deps)

        for ext_dep in ext_deps:
            console.print("")
            console.log(f"Building dependency package {ext_dep}",
                        style="bold cyan")
            if pacman.builder.build(ext_dep,
                                    force_configure=configure,
                                    clean=clean):
                console.log(f"{ext_dep}: build finished.", style="bold green")
            else:
                console.log(f"{ext_dep}: build failed.", style="bold red")
                return

    #for pkg in track(build_order):
    for pkg in build_order:
        console.print("")
        console.log(f"Building package {pkg}", style="bold cyan")
        #with console.status(f"Building package '{pkg}'") as status:
        if pacman.builder.build(pkg, force_configure=configure, clean=clean):
            console.log(f"{pkg}: build finished.", style="bold green")
        else:
            console.log(f"{pkg}: build failed.", style="bold red")

            import notify2 # pip install notify2 dbus-python
            notify2.init("ArmarX")
            n = notify2.Notification(f" Building package '{pkg.name}' failed.",
                                "",
                                "/home/fabi/repos/ArmarXCore/etc/templates/doxygen/ArmarXCore-Logo-failure.png"   # Icon name
                                )
            n.show()

            return

    import notify2 # pip install notify2 dbus-python
    notify2.init("ArmarX")
    n = notify2.Notification(" Build finished.",
                         "",
                         "/home/fabi/repos/ArmarXCore/etc/templates/doxygen/ArmarXCore-Logo.svg"   # Icon name
                        )
    n.show()