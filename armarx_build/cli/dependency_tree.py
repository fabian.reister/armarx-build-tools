import click
import networkx as nx
from matplotlib import pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

from armarx_build.core.package_manager import PackageManager
from armarx_build.cli import cli

@cli.command("dependency-tree")
@click.option("-p", "--package", 'packages', multiple=True, default=[])
@click.pass_obj
def cli_dependency_tree(pacman: PackageManager, packages):
    if not packages:
        packages = pacman.packages
    else:
        packages = pacman.package_finder.find(packages)

    packages = pacman.dependency_resolver.process_packages(packages, pacman.packages)

    pacman.console.print(packages)

    graph = pacman.dependency_resolver.dependency_graph(packages)

    pos = graphviz_layout(graph, prog="dot")
    nx.draw(graph, pos, with_labels=True, arrows=True, node_size=1000, node_shape="s")
    plt.show()