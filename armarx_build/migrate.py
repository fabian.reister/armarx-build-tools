import re


def migrate_cmakelists(file):

    with open(file, "r") as f:
        content = f.read()
        content = content.replace('\r', " ").replace('\n', " ")


    def find(var_name):
        # re_component_libs = re.compile(
        #     r'set\(COMPONENT_LIBS (?P<pkg>[\w\s]+)\)')

        # component_libs = re_component_libs.findall(content)

        re_var = re.compile(
                r'set\(\s*' + var_name +  r'\s+(?P<pkg>[\w\s.\/]+)\)')

        var = re_var.findall(content)

        if var:
            return var[0]

        return None

    re_var = re.compile(
                r'armarx_component_set_name\("*\s*(?P<pkg>[\w\s.]+)"*\)')

    var = re_var.findall(content)
    assert var
    component_name = var[0]

    #component_libs = component_libs[0].split(" ")
    component_libs = find(r"COMPONENT_LIBS")
    sources = find(r"SOURCES")
    headers = find(r"HEADERS")

    assert sources
    assert headers
    assert component_libs

    generate_statechart = "armarx_generate_statechart_cmake_lists()" in content

    print("component_libs", component_libs)
    print("sources", sources)
    print("headers", headers)
    print("generate_statechart", generate_statechart)

    headers = headers.split(' ')

    scgxmls = [h for h in headers if ".scgxml" in h]
    headers = [h for h in headers if ".scgxml" not in h]
    print("scgxmls", scgxmls)


    scgxmls = " ".join(scgxmls)
    headers = " ".join(headers)

    out = f"\narmarx__add_component({component_name} \n" 
    out += f"SOURCES {sources} \n"
    out += f"HEADERS {headers} \n"
    
    out += f"DEPENDENCIES {component_libs} \n"

    if generate_statechart:
        out += f"STATECHART_GROUP_FILE {scgxmls} \n" 

    out += ")"

    print("\n\n", out)

    with open(file, "w") as f:
       f.write(out)

    import os
    os.system(f"cmake-format {file} -i")



if __name__ == "__main__":
    main()
