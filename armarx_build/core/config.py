import toml


class Config:

    defaults = {
        "common" : {
            "workspace": "~/repos",
        },
        "packages": {
            "denylist": "",
        },
        "cmake": {
            "ccache": False,
            "args": "",
            "generator": "Makefile",
            "generator_args": "-j8",
            "gold-linker-denylist": "VisionX"
        }
    }

    def __init__(self, cfg_path: str):
        self.cfg = cfg_path

    def read(self):

        # parser = configparser.ConfigParser()
        # parser.read([self.cfg])
        # rv = {}
        # for section in parser.sections():
        #     rv[section] = {}
        #     for key, value in parser.items(section):
        #         rv[section][key] = os.path.expandvars(value)


        # print("Loading config ", self.cfg)
        rv = toml.load(self.cfg)

        for section in self.defaults.keys():
            if section in rv.keys():
                self.defaults[section].update(rv[section])



        return self.defaults