import glob
import os

from dataclasses import dataclass

import click
from rich.console import Console

from armarx_build.core.config import Config
from armarx_build.core.types import Package
from armarx_build.core.cmake_package_finder import CMakePackageFinder
from armarx_build.core.dependency_resolver import DependencyResolver
from armarx_build.core.cmake_builder import CMakeBuilder

class PackageManager:

    console: Console

    APP_NAME = 'armarx'

    @property
    def config_filename(self):
        app_dir = click.get_app_dir(self.APP_NAME)
        cfg = os.path.join(app_dir, 'config.toml')
        return cfg

    def __init__(self, console: Console):

        self.console = console
        # read the config
        cfg = self.config_filename

        if not os.path.exists(cfg):
            self.console.print(f"Config {cfg} does not exist!", style="red")
            return

        self.config = Config(cfg).read()

        self.dependency_resolver = DependencyResolver(
            config=self.config["packages"], console=self.console)
        self.builder = CMakeBuilder(self.config, console=self.console)
        self.package_finder = CMakePackageFinder(
            config=self.config["packages"], console=self.console)

    @property
    def packages(self):
        """
        List of all packages, but dependencies are not populated
        """

        return self.package_finder.discover_all()

    @property
    def default_packages(self):

        all_packages = self.packages

        default_list = self.config["packages"]["default_packages"]
        default_packages = [
            pkg for pkg in all_packages if pkg.name in default_list
        ]

        return default_packages

    @property
    def external_dependencies(self):
        external_deps_list = self.config["packages"][
            "external_dependencies"]

        external_deps = []

        for ext_dep in external_deps_list:
            cmake_pkg_export_dir = os.path.expanduser(
                os.path.join("~/.cmake/packages", ext_dep))

            if not os.path.exists(cmake_pkg_export_dir):
                self.console.print(f"Dependency {ext_dep} not found!",
                              style="red bold")
                continue

            files = glob.glob(os.path.join(cmake_pkg_export_dir, "*"))
            files = list(filter(os.path.isfile, files))

            for file in files:
                with open(file) as f:
                    build_dir_link = str(f.readline())

                    path = os.path.dirname(build_dir_link)
                    external_deps.append(Package(name=ext_dep, path=path))

        return external_deps