import glob
import os
import re
from dataclasses import dataclass
from typing import Dict, List

from rich.console import Console

from armarx_build.core.types import Package

ARMARX_WORKSPACE_ENV_VAR = "ARMARX_WORKSPACE"

@dataclass
class CMakePackageFinder:

    config: Dict  # section 'packages'
    console: Console

    def discover_all(self):

        try:
            workspace = os.environ[ARMARX_WORKSPACE_ENV_VAR]
        except:
        #if not workspace:
            self.console.print(f"The environment variable *{ARMARX_WORKSPACE_ENV_VAR}* is not set!", style="red")
            exit(-1)

        workspace = os.path.abspath(os.path.expanduser(workspace))
        denylist = self.config["denylist"]

        # https://stackoverflow.com/questions/7159607/list-directories-with-a-specified-depth-in-python
        search_directories1 = glob.glob(os.path.join(workspace, '*'))
        search_directories2 = glob.glob(os.path.join(workspace, '*/*'))
        search_directories3 = glob.glob(os.path.join(workspace, '*/*/*'))
        search_directories4 = glob.glob(os.path.join(workspace, '*/*/*/*'))
        #search_directories5 = glob.glob(os.path.join(workspace, '*/*/*/*/*'))

        search_directories = search_directories1 + search_directories2 + search_directories3 + search_directories4 # + search_directories5

        search_directories = filter(lambda f: os.path.isdir(f),
                                    search_directories)

        search_directories = list(search_directories)

        re_xproject = re.compile(
            r'armarx_project\([\s\"]*(?P<pkg>\w+)[\s\"]*\)')

        # armarx_project(NAME control NAMESPACE armarx)
        re_xproject_v2 = re.compile(
            r'armarx_project\((?P<pkg>\w+)[\w\s_]*\)')

        re_xproject_v2_namespace = re.compile(
            r'armarx_project\((?P<pkg>\w+)\s+NAMESPACE\s+(?P<namespace>\w+)\)')

        def as_armarx_package(path):
            cmake_lists = os.path.join(workspace, path, "CMakeLists.txt")

            if not os.path.exists(cmake_lists):
                return None

            if not os.path.exists(os.path.join(workspace, path, "source")):
                return None

            if not os.path.exists(os.path.join(workspace, path, "data")):
                return None

            if not os.path.exists(os.path.join(workspace, path, "etc")):
                return None

            with open(cmake_lists) as cmake_lists_file:
                try:
                    content = cmake_lists_file.read()
                except:
                    return None

                content = content.replace('\r', " ").replace('\n', " ")

                # replace multiple whitespaces by one
                content = ' '.join(content.split())

                matches = re_xproject.findall(content)
                matches_v2 = re_xproject_v2.findall(content)
                matches_v2_namespace = re_xproject_v2_namespace.findall(content)

                if not matches and not matches_v2 and not matches_v2_namespace:
                    return None

                #assert len(matches), f"armarx_project(...) should appear exactly once in {cmake_lists}"
                # + len(matches_v2)) == 1, f"armarx_project(...) should appear exactly once in {cmake_lists}"

                if matches:
                    pkg = matches[0]

                if matches_v2:
                    pkg = matches_v2[0]

                if matches_v2_namespace:
                    #console.log(matches_v2_namespace)
                    #pkg = matches_v2_namespace[0]
                    pkg = '_'.join([matches_v2_namespace[0][1],matches_v2_namespace[0][0]])

                path = os.path.dirname(cmake_lists)

                # console.print(f"Found package {pkg} at {path}")

                return Package(name=pkg, path=path)

        def is_package_enabled(pkg: Package):
            return pkg.name not in denylist

        search_directories.sort()

        packages = map(as_armarx_package, search_directories)



        # packages now contains some None elements
        packages = filter(lambda p: p is not None, packages)
        packages = filter(is_package_enabled, packages)

        return list(packages)

    def find(self, pkgs: List[str]):
        known_packages = self.discover_all()

        def find_single(pkg):

            for known_package in known_packages:
                if known_package.name == pkg:
                    return known_package

            self.console.print("Cound not find package ", pkg)
            return None

        return list(map(find_single, pkgs))
