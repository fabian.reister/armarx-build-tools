from dataclasses import dataclass, field
from typing import Set


@dataclass
class Dependencies:
    required: Set[str] = field(default_factory=set)
    optional: Set[str] = field(default_factory=set)

    @property
    def all(self):
        return set(list(self.required) + list(self.optional))


@dataclass
class Package:
    name: str
    path: str

    dependencies: Dependencies = Dependencies()

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(str(self.name))

    def __eq__(self, other):
        return self.name == other.name