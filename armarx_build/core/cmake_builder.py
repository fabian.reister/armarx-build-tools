import os
from dataclasses import dataclass
from typing import Dict

from armarx_build.core.types import Package
from rich.console import Console


@dataclass
class CMakeBuilder:

    config: Dict
    console: Console

    def _needs_configure(self, package: Package):
        build_dir = os.path.join(package.path, "build")
        return not os.path.exists(os.path.join(
            build_dir, "CMakeCache.txt")) or not os.path.exists(
                os.path.join(build_dir, "build.ninja"))

    def _configure(self, package: Package):
        cmake_config = self.config["cmake"]

        # console.print(self.config)

        generator = cmake_config["generator"]
        cmake_args : list = cmake_config["args"]  # without -D !

        # build_dir = os.path.join(package.path, "build")

        if package.name in self.config["cmake"].keys():
           package_config : dict = self.config["cmake"][package.name]

           self.console.print(package_config)

           if "args" in package_config.keys():
               cmake_args.extend(package_config["args"])

        cmake_expanded_args = ""
        for cmake_arg in cmake_args:
            cmake_expanded_args += f" -D{cmake_arg} "

        gold_linker_denylist = self.config["cmake"][
            "gold-linker-denylist"]

        if package.name not in gold_linker_denylist:
            cmake_expanded_args += " -DARMARX_USE_GOLD_LINKER=1 "

        # cmake_cfg_cmd = f"cmake -B{build_dir} -S{package.path} -G {generator} {cmake_expanded_args} "
        cmake_cfg_cmd = f"cd {package.path}/build && cmake .. -G {generator} {cmake_expanded_args} "

        self.console.log("[yellow]Executing:[/yellow]", cmake_cfg_cmd)

        self.console.print("")
        result = os.system(cmake_cfg_cmd)
        self.console.print("")

        return result == 0
        #console.print(result)

    def _build(self, package: Package, clean: bool):
        build_dir = os.path.join(package.path, "build")

        cmake_config = self.config["cmake"]
        generator_args = cmake_config["generator_args"]

        clean_cmd = " --clean-first " if clean else ''

        cmake_build_cmd = f"cmake --build {build_dir} {clean_cmd} -- {generator_args}"
        self.console.log("[yellow]Executing:[/yellow]", cmake_build_cmd)

        self.console.print("")
        result = os.system(cmake_build_cmd)
        self.console.print("")

        return result == 0

    def build(self, package: Package, force_configure: bool, clean: bool):

        denylist = self.config["packages"]["denylist"]

        if package.name in denylist:
            self.console.print(
                f"Skipping package {package.name} as it is in denylist")
            return True

        if force_configure or self._needs_configure(package):
            if not self._configure(package):
                self.console.print(f"Configure failed (package {package})!",
                              style="bold red")
                return False

        return self._build(package, clean)