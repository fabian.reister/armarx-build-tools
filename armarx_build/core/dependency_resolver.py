import os
import re
from dataclasses import dataclass
from typing import Dict, List, OrderedDict

import networkx as nx
from rich.console import Console

from armarx_build.core.types import Dependencies, Package


@dataclass
class DependencyResolver:

    config: Dict  # packages section
    console: Console

    def _exists(self, package: Package):
        pkg_cmake_lists = os.path.join(package.path, "CMakeLists.txt")

        if not os.path.exists(pkg_cmake_lists):
            self.console.print(f"Dependency {package} does not exist!", style="red")
            return False
        return True

    def _resolve_dependencies(self, package: Package):

        pkg_cmake_lists = os.path.expanduser(
            os.path.join(package.path, "CMakeLists.txt"))

        assert self._exists(package), f"Package {package} does not exist"

        with open(pkg_cmake_lists) as cmake_lists_file:

            content = cmake_lists_file.read()

            re_required_pkg_legacy = re.compile(
                r"depends_on_armarx_package\(\s*(?P<pkg>[:\w]+)\s*\)")

            # re_required_pkg = re.compile(
            #    r"armarx_find_package\(\s*(?P<scope>\w+)\s*(?P<pkg>[:\w]+)\s*.*\)")

            re_required_pkg = re.compile(
                r"armarx_find_package\(\s*\w+\s*(?P<pkg>[:\w]+)\s*.*\)")

            re_optional_pkg_legacy = re.compile(
                r'depends_on_armarx_package\(\s*(?P<pkg>[:\w]+)\s*"OPTIONAL"\s*\)'
            )

            re_optional_pkg = re.compile(
                r'armarx_depends_on\(\s*ARMARX\s*(?P<pkg>[:\w]+)\s*"OPTIONAL"\s*\)'
            )

            def is_not_cycle(pkg):
                return pkg != package.name

            required_packages = re_required_pkg.findall(content) + re_required_pkg_legacy.findall(content)
            required_packages = list(filter(is_not_cycle, required_packages))

            def to_x_package_name(name: str):
                splits = name.split("::")
                if len(splits) > 1:
                    return '_'.join(splits)
                else:
                    return name

            required_packages = list(map(to_x_package_name, required_packages))

            if package.name != "ArmarXCore":
                required_packages.append("ArmarXCore")

            optional_packages = re_optional_pkg.findall(content) + re_optional_pkg_legacy.findall(content)
            optional_packages = list(filter(is_not_cycle, optional_packages))

            required_packages = set(required_packages)
            optional_packages = set(optional_packages)

            return Dependencies(
                required=required_packages,
                optional=optional_packages,
            )

    def process_packages(self, pkgs: List[Package],
                         known_packages: List[Package]):

        packages = set()
        roots = set()

        known_package_paths = dict([(pkg.name, pkg.path)
                                    for pkg in known_packages])

        def visit(pkg: Package):

            if not self._exists(pkg):
                return

            pkg.dependencies = self._resolve_dependencies(pkg)

            if not pkg.dependencies.all:  # empty list
                roots.add(pkg)
                packages.add(pkg)
                return

            for dep in pkg.dependencies.required:

                if not dep in known_package_paths.keys():
                    continue

                path = known_package_paths[dep]
                p = Package(name=dep, path=path)

                if not p in packages:
                    packages.add(p)
                    visit(p)

            if self.config["use_optional_packages"]:
                for dep in pkg.dependencies.optional:

                    if not dep in known_package_paths.keys():
                        continue

                    path = known_package_paths[dep]
                    p = Package(name=dep, path=path)

                    if not p in packages:
                        packages.add(p)
                        visit(p)

            packages.add(pkg)

        for package in pkgs:
            visit(package)

        return packages

    def dependency_graph(self, packages: List[Package]):

        pkgs = dict([package.name, package] for package in packages)

        def recursive_dependencies(package_name: str):
            deps = set()

            if package_name in pkgs.keys():
                package = pkgs[package_name]

                for pkg in package.dependencies.all:
                    deps |= recursive_dependencies(pkg)

                    # if not top_level:
                    deps.add(pkg)
            else:
                print(f"package {package_name} unknown")

            return deps



        G = nx.DiGraph()

        for pkg in packages:
            G.add_node(pkg.name)

            self.console.print("Package!!! ", pkg)

            rdeps = set()

            for dep in pkg.dependencies.all:
                rdeps |= recursive_dependencies(dep)

            for dep in pkg.dependencies.all:
                if not dep in rdeps:
                    G.add_edge(pkg.name, dep)
                else:
                    self.console.print("skipping ", dep, rdeps)

            # if self.config["use_optional_packages"]:
            #     for dep in pkg.dependencies.optional:
            #         G.add_edge(pkg.name, dep)

        return G

    def build_order(self, packages: List[Package]):

        # build order
        build_order = []

        use_optional_packages = self.config["use_optional_packages"]

        known_package_names = set([pkg.name for pkg in packages])

        # brute force solution
        packages_to_process = packages
        while packages_to_process:

            n_to_do = len(packages_to_process)

            for package in packages_to_process:

                # all dependecies processed
                required_dependencies_processed = [
                    dep in build_order for dep in package.dependencies.required
                    if dep in known_package_names
                ]
                optional_dependencies_processed = [
                    dep in build_order for dep in package.dependencies.optional
                    if dep in known_package_names
                ]

                dependencies_processed = required_dependencies_processed + optional_dependencies_processed if use_optional_packages else required_dependencies_processed

                if all(dependencies_processed):
                    build_order.append(package.name)

                # package without deps
                if len(dependencies_processed) == 0:
                    build_order.append(package.name)

            # remove processed packages
            packages_to_process = [
                pkg for pkg in packages_to_process
                if pkg.name not in build_order
            ]

            new_n_to_do = len(packages_to_process)

            if n_to_do == new_n_to_do:
                console.print("Cannot create build order!")
                console.print(packages_to_process)

                for pkg in packages_to_process:
                    console.print(f"Package {pkg}")
                    console.print("Unresolved deps: ")
                    unresolved_deps = [
                        dep for dep in pkg.dependencies.required
                        if dep not in build_order
                    ]
                    console.print(unresolved_deps)

                break

            n_to_do = new_n_to_do

        pkgs = dict([(pkg.name, pkg) for pkg in packages])

        # "Ordered set" => make sure, packages get only built once
        build_order = OrderedDict([b, None] for b in build_order).keys()

        bo = [pkgs[b] for b in build_order]

        return bo
