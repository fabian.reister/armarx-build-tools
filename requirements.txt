setuptools~=57.1.0
click~=7.1.2
rich~=9.13.0
typing~=3.7.4.3
networkx~=2.5
matplotlib~=3.3.4
toml~=0.10.2