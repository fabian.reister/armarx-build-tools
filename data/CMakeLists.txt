armarx_component_set_name("Armar6ActiveImpedanceGroup")

set(COMPONENT_LIBS
    RobotAPICore RobotStatechartHelpers
    Armar6RTInterfaces
    ArmarXCoreStatechart ArmarXCoreObservers)

set(SOURCES Armar6ActiveImpedanceGroupRemoteStateOfferer.cpp)
set(HEADERS Armar6ActiveImpedanceGroupRemoteStateOfferer.h Armar6ActiveImpedanceGroup.scgxml)

armarx_generate_statechart_cmake_lists()

armarx_add_component("${SOURCES}" "${HEADERS}")
